# SoftwareHuset A/S

## Om projektet 
Dette projekt går ud på at lave et program som kan gøre det nemmere for SoftwareHuset A/S. 

## Guide til programmet 
Vi har lavet et console UI til vores program, for at starte programmet skal filen [UI](src/main/java/ui/UI.java) køres.
Når programmet startes vil der komme en menu frem hvor man kan vælge mellem *developer menu* eller *project menu*. 


## Gruppe 08
- Christian Emil Tchernokojev Houmann -- s194602 @194602
- Joachim Touveneau Petersen -- s194296 @JoachimTepe
- Regin Steffansson Kunoy -- s183462 @OGummi
- Loui Nissen-Petersen -- s194294 @ItzLue
